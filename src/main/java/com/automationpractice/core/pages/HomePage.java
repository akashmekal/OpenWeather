package com.automationpractice.core.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.automationpractice.core.base.ElementActions;
import com.automationpractice.core.base.Globals;

public class HomePage {

	private ElementActions elementActions = new ElementActions();

	@FindBy(xpath = "//img[@alt='openweather']")
	private WebElement OpenWeatherMapLogo;

	@FindBy(xpath = "(//a[contains(.,'Sign In')])[1]")
	private WebElement SignIn;

	@FindBy(xpath = "//a[contains(.,'Sign Up')]")
	private WebElement SignUp;

	@FindBy(xpath = "//button[contains(.,'Search')]")
	private WebElement SearchCityButton;

	@FindBy(xpath = "(//input[contains(@type,'text')])[2]")
	private WebElement CityNameSearchButton;

	@FindBy(xpath = "(//div[contains(.,'×Not found')])[6]")
	private WebElement CityNotFound;

	@FindBy(xpath = "//span[contains(.,'28.5°С')]")
	private WebElement CityFound;


	public HomePage(){

		PageFactory.initElements(Globals.getDriver(), this);
	}


	public void HomePageAssertionLogo() {
		Assert.assertEquals("Сurrent weather and forecast - OpenWeatherMap", Globals.getDriver().getTitle());
		if (elementActions.isElementDisplay(OpenWeatherMapLogo, 15))
			System.out.println("OpenWeatherMapLogo is been displayed");
		else{
			System.out.println("OpenWeatherMapLogo is not been displayed");

		}

	}

	public void HomePageAssertionSignUpSignIn() {
		if (elementActions.isElementDisplay(SignIn, 15))
			System.out.println("SignIn Button is been displayed");
		else{
			System.out.println("SignIn Button is not been displayed");

		}

		if (elementActions.isElementDisplay(SignUp, 15))
			System.out.println("SignUp Button is been displayed");
		else{
			System.out.println("SignUp Button is not been displayed");

		}

	}

	public void HomePageAssertionSearchCity() {
		if (elementActions.isElementDisplay(SearchCityButton, 15))
			System.out.println("SearchCityButton is been displayed");
		else{
			System.out.println("SearchCityButton is not been displayed");

		}

	}

	public void CitySearch(String InvCity) {
		elementActions.ElementClick(CityNameSearchButton);
		elementActions.TypeElement(CityNameSearchButton, InvCity);
		elementActions.ElementClick(SearchCityButton);
	}

	public void CityNotFound() {
		if (elementActions.isElementDisplay(CityNotFound, 15))
			System.out.println("Desired City is not a valid city, please enter an valid City Name");
	}

	public void WeatherReport() {
		List<WebElement> elements = Globals.getDriver().findElements(By.id("forecast_list_ul"));
		for (WebElement element : elements) {
			if(element.getText().contains("Not found")) {
				System.out.println("Seems like you have entered an InValid City, Please try again with a Valid City Name");}
			else {
			System.out.println(element.getText());}

		}
	}

}