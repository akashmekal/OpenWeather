Feature: This is an acceptance criteria to assert values on HomePage
	
  @HomePageAssertions
  Scenario:  User Lands on Home Page of Open Weather Map website and assert few fields
  
  			Given I am on Home Page
  			Then I verify that the OpenWeatherMap logo is persent on HomePage
  			Then I verify the Signup and SignIn button is persent on HomePage
  			Then I verify the Search Button on HomePage where user can Search any valid City
  			