package com.automationpractice.core.utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automationpractice.core.base.Globals;
import com.automationpractice.core.utils.APICalls;

@SuppressWarnings("deprecation")
public class APITest {
	
	private WebDriver driver;
	private String baseUrl;
	private String apiurl;
	@BeforeClass
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver","C://AkashMekal//MainFrame//automationpractice//src//test//resources//drivers//chromedriver.exe");
		driver = new ChromeDriver();
		System.out.println("API Under Test is:" + " " +  apiurl);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	}


	@AfterClass
	public void tearDown() throws Exception {
		driver.close();
		driver.quit();
	}

	@Test
	public void test() throws ClientProtocolException, IOException {
		baseUrl = new ConfigFileReader().getProperty("baseapiurl");
		apiurl = new ConfigFileReader().getProperty("apiurl");
		System.out.println("API Under Test is:" + " " +  apiurl);
		Globals.getDriver().get(baseUrl);
		Globals.getDriver().navigate().to(apiurl);	
		WebElement webElement=Globals.getDriver().findElement(By.tagName("pre"));
		APICalls weatherApiResponse = new APICalls();
		String ExpectedString=weatherApiResponse.GetResponse();
	}

}

