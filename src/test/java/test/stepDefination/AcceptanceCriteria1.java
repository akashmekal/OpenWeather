package test.stepDefination;



import com.automationpractice.core.base.Globals;
import com.automationpractice.core.pages.HomePage;
import com.automationpractice.core.utils.ConfigFileReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class AcceptanceCriteria1 {


	@Given("^I am on Home Page$")
	public void i_am_on_HomePage() throws Throwable {
		Globals.getDriver().get(new ConfigFileReader().getProperty("testurl"));
	}

	@Then("I verify that the OpenWeatherMap logo is persent on HomePage")
	public void i_verify_that_the_OpenWeatherMap_logo_is_persent_on_HomePage() {
		new HomePage().HomePageAssertionLogo();

	}

	@Then("I verify the Signup and SignIn button is persent on HomePage")
	public void i_verify_the_Signup_and_SignIn_button_is_persent_on_HomePage() {
		new HomePage().HomePageAssertionSignUpSignIn();

	}
	
	@Then("I verify the Search Button on HomePage where user can Search any valid City")
	public void i_verify_the_Search_Button_on_HomePage_where_user_can_Search_any_valid_City() {
		new HomePage().HomePageAssertionSearchCity();
	
	}

}