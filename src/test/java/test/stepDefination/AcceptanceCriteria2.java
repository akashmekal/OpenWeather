package test.stepDefination;

import com.automationpractice.core.pages.HomePage;

import cucumber.api.java.en.Then;

public class AcceptanceCriteria2 {
	
	@Then("I enter {string} in the Your City Name search box and i click on the search button")
	public void i_enter_in_the_Your_City_Name_search_box(String string) {
		new HomePage().CitySearch(string); 
	}

	@Then("I valid the error thrown by the system")
	public void i_valid_the_error_thrown_by_the_system() {
		new HomePage().WeatherReport();
	}

}
