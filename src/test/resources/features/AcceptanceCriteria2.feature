Feature: This is an acceptance criteria to valid an invalid City Search
	
  @InvalidCitySearch
  Scenario Outline:  User Lands on Home Page of Open Weather Map website and searchs and invalid city
  
  			Given I am on Home Page
  			Then I enter "<InvalidCityName>" in the Your City Name search box and i click on the search button
  			Then I valid the error thrown by the system
  			
  			
  			
  			
  Examples:
 |InvalidCityName|
 |	Mekal |