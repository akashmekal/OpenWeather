package test.stepDefination;

import com.automationpractice.core.utils.APITest;
import cucumber.api.java.en.Given;

public class OpenWeatherAPI {


	@Given("I open the website, test the api and display the response code")
	public void i_open_the_website_test_the_api_and_display_the_response_code() throws Exception {
		new APITest().test();	
	}
}
